package org.example;

import org.example.model.Slot;
import org.example.model.User;
import org.example.model.Vaccine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class UserTest {
    @Mock
    Slot slotmock;

    @Test
    public void emptyUserConstructorTest(){
        User user = new User();
        assertEquals("",user.getName());
        assertEquals("",user.getMob());
        assertEquals("",user.getAadhar());
        assertEquals(0,user.getDosesTaken());
        assertEquals(0,user.getAge());
        assertEquals(Vaccine.Null,user.getVaccine());

    }
//    @Test
//    public void getDosesTaken() {
//        //User user = new User();
//
//    }
//
//    @Test
//    public void setDosesTaken() {
//        User user = new User();
//        user.setDosesTaken(0);
//
//    }
//
//    @Test
//    public void getVaccine() {
//    }
//
//    @Test
//    public void setVaccine() {
//    }
//
//    @Test
//    public void getAge() {
//    }
//
//    @Test
//    public void setAge() {
//    }
//
    @Test
    public void getSlot() {
        User user = new User("Jayadev", "9447445511", "12345677");
        assertNotNull(slotmock);
        user.setSlot(slotmock);
        assertEquals(user.getSlot(),slotmock);


    }
//
//    @Test
//    public void testToString() {
//    }
//
//    @Test
//    public void getName() {
//    }
//
//    @Test
//    public void setName() {
//    }
//
//    @Test
//    public void getMob() {
//    }
//
//    @Test
//    public void setMob() {
//    }
//
//    public void getAadhar() {
//    @Test
//    }
//
//    @Test
//    public void setAadhar() {
//    }
//
//    @Test
//    public void status() {
//    }
//
//    @Test
//    public void vaccinate() {
//    }
}