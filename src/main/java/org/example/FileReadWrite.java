package org.example;

import org.example.model.Location;
import org.example.model.User;
import org.example.model.Vaccentre;
import org.example.model.Vaccine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileReadWrite implements FileOps {
    public ArrayList<User> readFromUserFile(ArrayList<User> userDB){
        try(InputStream in = App.class.getResourceAsStream("/Users.txt")){
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String line;
            while((line=br.readLine())!=null){
                String[] parts = line.split(",");
                User user = new User();
                user.setMob(parts[0]);
                user.setName(parts[1]);
                user.setAadhar(parts[2]);
                user.setAge(Integer.parseInt(parts[3]));
                user.setDosesTaken(Integer.parseInt(parts[4]));
                user.setVaccine(Vaccine.valueOf(parts[5]));
                System.out.println(user.toString());
                System.out.println("Hey");
                userDB.add(user);


            }
            br.close();

        }

        catch (NullPointerException| IOException e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
        return userDB;
    }

    public ArrayList<Vaccentre> readFromVacFile(ArrayList<Vaccentre> vaccentres){
        try(InputStream in = App.class.getResourceAsStream("/Vaccentres.txt")){
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String line;
            while((line=br.readLine())!=null){
                String[] parts = line.split(",");
                Vaccentre vaccentre = new Vaccentre();
                vaccentre.setName(parts[0]);
                vaccentre.setVaccine(Vaccine.valueOf(parts[1]));
                Location loc  = new Location();
                loc.setLocality(parts[2]);
                loc.setDist(parts[3]);
                loc.setState(parts[4]);
                loc.setPincode(parts[5]);
                vaccentre.setLocation(loc);
                vaccentre.setStock(Integer.parseInt(parts[6]));
                System.out.println(vaccentre);
                vaccentres.add(vaccentre);

            }
            br.close();

        }

        catch (NullPointerException|IOException e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
        return vaccentres;
    }


    public void writeToUserFile(List<User> users){
        try(FileWriter myWriter = new FileWriter(String.valueOf((User.class.getResource("User.txt"))))){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(User user : users) {
                bw.write(user.getMob() + "," + user.getName() + "," + user.getAadhar() + "," + user.getAge()  + "," + user.getDosesTaken() + "," + user.getVaccine());
                bw.newLine();
                System.out.println(user.toString());

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }

    public void writeToVacFile(List<Vaccentre> vaccentres){
        try(FileWriter myWriter = new FileWriter("Vaccentres.txt")){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(Vaccentre vaccentre : vaccentres) {
                bw.write(vaccentre.getName() + "," + vaccentre.getVaccine() + "," + vaccentre.getLocation().toString() + "," + vaccentre.getStock());
                bw.newLine();

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }
}
