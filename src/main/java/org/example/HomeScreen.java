package org.example;

import org.example.model.User;
import org.example.model.Vaccentre;

import java.util.List;
import java.util.Scanner;

public class HomeScreen {
    public void start(List<User> userDB, List<Vaccentre> vaccentreDB, Scanner scanner){
        while(true) {
            System.out.println("New Registration? : Press 1");
            System.out.println("User Login?: Press 2");
            System.out.println("Admin Login? Press 3, Press 4 to exit");
            int ans = Integer.valueOf(scanner.nextLine());
            if (ans == 1) {
                Registration reg = new Registration();
                reg.start(userDB,scanner);
            } else if (ans == 2) {
                Login log = new Login();
                log.start(userDB,vaccentreDB,scanner);
            } else if (ans == 3) {
                AdminDash admin = new AdminDash();
                admin.start(vaccentreDB,scanner);
            }
            else if(ans == 4){
                break;
            }
        }

    }
}
