package org.example;

import org.example.model.Location;
import org.example.model.Slot;
import org.example.model.User;
import org.example.model.Vaccentre;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class UserDash {
    public void start(List<User> userDB, List<Vaccentre> vaccentreDB, Scanner scanner, User user) {
        while (true) {
            System.out.println("Schedule Vaccination. Press 1.");
            System.out.println("Vaccination Status. Press 2");
            System.out.println("Logout. Press 3");
            int input = Integer.valueOf(scanner.nextLine());
            if(input == 3)
                break;
            if (input == 1) {
                //while(true) {
                    System.out.println();
                    Location loc = new Location();
                    Location temploc = new Location();
                    loc = temploc.getLocation(scanner);
                    ArrayList<Vaccentre> nearbys = loc.getNearestVaccentres(vaccentreDB).stream().filter(vaccine -> vaccine.getStock()>0).collect(Collectors.toCollection(ArrayList::new));
                    //System.out.println(nearbys);
                    for (Vaccentre vacc : nearbys) {
                        System.out.println(vacc.getStatus());
                    }while(true) {
                        System.out.println("Enter name of vaccentre to Schedule: Press exit to exit");
                        String name = scanner.nextLine();
                        if(name.equals("exit"))
                            break;
                        Vaccentre vaccentre = findVaccenter(name, vaccentreDB);
                    System.out.println("Enter date : (DD/mm/yyy");
                    String datestr = scanner.nextLine();
                    Date date = new Date();
                    try {
                        date = new SimpleDateFormat("dd/MM/yyy").parse(datestr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Enter time : (Morning/Afternoon)");
                    String slottime = scanner.nextLine();



                        Slot slot = new Slot(date,slottime,vaccentre);


                        if (vaccentre == null) {
                            System.out.println("Re-enter");
                            continue;
                        }
                        else{
                            if(vaccentre.schedule(user,slot))
                                break;
                            else {
                                System.out.println("Failed. Try again");
                            }
                        }

                   // }
                }


            }
        }

    }

    public Vaccentre findVaccenter(String name, List<Vaccentre> vaccentres){
        for(Vaccentre vacc : vaccentres){
            if(vacc.getName().equals(name))
                return vacc;
        }
        return null;
    }



}
