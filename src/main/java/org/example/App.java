package org.example;

import org.example.model.User;
import org.example.model.Vaccentre;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {   ArrayList<User> userDB = new ArrayList<>();
        ArrayList<Vaccentre> vaccentreDB = new ArrayList<>();
        FileOps fileops = new FileReadWrite();
        ArrayList<User> users = fileops.readFromUserFile(userDB);
        ArrayList<Vaccentre> vaccentres = fileops.readFromVacFile(vaccentreDB);

        //readFromUserFile(userDB);
        //readFromVacFile(vaccentreDB);
        HomeScreen home = new HomeScreen();
        Scanner scanner = new Scanner((System.in));
        home.start(userDB,vaccentreDB, scanner );
        System.out.println("Exited Home");
        fileops.writeToUserFile(users);
        fileops.writeToVacFile(vaccentres);
    }

    /*public static void readFromUserFile(List<User> userDB, ClassLoader classLoader){

        try(Scanner reader = new Scanner(Paths.get(String.valueOf(classLoader.getResource("src/main/resources/Users.txt"))))){

            while(reader.hasNextLine()){
                String[] parts = reader.nextLine().split(",");
                User user = new User();
                user.setMob(parts[0]);
                user.setName(parts[1]);
                user.setAadhar(parts[2]);
                user.setAge(Integer.parseInt(parts[3]));
                user.setDosesTaken(Integer.parseInt(parts[4]));
                user.setVaccine(Vaccine.valueOf(parts[5]));
                userDB.add(user);

            }

        }

        catch (IOException e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
    }

    public static void readFromVacFile(List<Vaccentre> vaccentres, ClassLoader classLoader){
        try(Scanner reader = new Scanner(Paths.get("src/test/resources/Vaccentres.txt"))){
            while(reader.hasNextLine()){
                String[] parts = reader.nextLine().split(",");
                Vaccentre vaccentre = new Vaccentre();
                vaccentre.setName(parts[0]);
                vaccentre.setVaccine(Vaccine.valueOf(parts[1]));
                Location loc  = new Location();
                loc.setLocality(parts[2]);
                loc.setDist(parts[3]);
                loc.setState(parts[4]);
                loc.setPincode(parts[5]);
                vaccentre.setLocation(loc);
                vaccentre.setStock(Integer.parseInt(parts[6]));
                vaccentres.add(vaccentre);

            }
        }

        catch(Exception e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
    }

    public static void writeToUserFile(List<User> users, String file){
        try(FileWriter myWriter = new FileWriter(file)){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(User user : users) {
                bw.write(user.getMob() + "," + user.getName() + "," + user.getAadhar() + "," + user.getAge()  + "," + user.getDosesTaken() + "," + user.getVaccine());
                bw.newLine();

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }

    public static void writeToVacFile(List<Vaccentre> vaccentres,String file){
        try(FileWriter myWriter = new FileWriter(file)){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(Vaccentre vaccentre : vaccentres) {
                bw.write(vaccentre.getName() + "," + vaccentre.getVaccine() + "," + vaccentre.getLocation().toString() + "," + vaccentre.getStock());
                bw.newLine();

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }*/
}
