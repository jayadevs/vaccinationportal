package org.example;

import org.example.model.User;
import org.example.model.Vaccentre;

import java.util.List;
import java.util.Scanner;

public class Login {
    private String mob;
    private int otp;
    public Login(){
        this.mob = "";
        this.otp = -999;

    }
    public void start(List<User> userDB, List<Vaccentre> vaccentreDB, Scanner scanner){

        while(true) {
            System.out.print("Mobile no: ");
            String mob = scanner.nextLine();
            User user = isinDB(userDB,mob);
            if(user!=null) {
                System.out.print("OTP: ");
                String otp = scanner.nextLine();
                UserDash dash = new UserDash();
                dash.start(userDB,vaccentreDB,scanner,user);
                break;

            }
            else{
                System.out.println("User not found. Please register yourself");
                break;


            }

        }



    }
    public User isinDB(List<User> userDB, String mob){
        for(User user : userDB){
            if(user.getMob().equals(mob))
                return user;
        }
        return null;
    }


}
