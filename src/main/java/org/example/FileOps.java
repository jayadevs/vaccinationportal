package org.example;

import org.example.model.User;
import org.example.model.Vaccentre;

import java.util.ArrayList;
import java.util.List;

public interface FileOps {
    ArrayList<User> readFromUserFile(ArrayList<User> userDB);
    ArrayList<Vaccentre> readFromVacFile(ArrayList<Vaccentre> vaccentreDB);
    void writeToUserFile(List<User> users);

    void writeToVacFile(List<Vaccentre> vaccentres);
}
