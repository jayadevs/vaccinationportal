package org.example.model;
import java.util.HashMap;
import java.util.Map;

public class Vaccentre{
    private String name;
    private Location location;
    private int stock;
    private Vaccine vaccine;
    private Map<User,Slot> scheduled;

    public Vaccentre(String name, Vaccine vaccine, Location location){
        this.name = name;
        this.location = location;
        this.vaccine = vaccine;
        this.scheduled = new HashMap<>();
        System.out.println("Vaccentre added successfully");
    }
    public Vaccentre(){
        this.name = " ";

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    public String getStatus(){
        System.out.println("Name of Center   Vaccine   Address   Stock");
        return this.name + "  "  + this.location.toString();
    }


    public void addStock(int stock){
        if(stock>0)
            this.stock+=stock;
    }
    public void removeStock(int stock){
        if(stock>0 && this.stock>=stock)
            this.stock-=stock;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }



    public boolean schedule(User user,Slot slot){
        if(scheduled.size()<this.stock && !scheduled.containsKey(user)){
            scheduled.put(user,slot);
            user.setSlot(slot) ;
            return true;
        }
        return false;

    }
    public void vaccinate(User user){
        if(scheduled.containsKey(user) && this.getStock()>0){
            user.setDosesTaken(user.getDosesTaken()+1);
            scheduled.remove(user);
            this.removeStock(this.getStock()-1);
        }
    }
}
