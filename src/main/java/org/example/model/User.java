package org.example.model;

import java.util.ArrayList;
import java.util.List;

import static org.example.model.Vaccine.Null;

public class User {
    private String name;
    private String mob;
    private String aadhar;
    private int age;
    private int dosesTaken;
    private Vaccine vaccine;
    private List<User> connections;
    private Slot slot;

    public User(String name, String mob, String aadhar) {
        this.name = name;
        this.mob = mob;
        this.aadhar = aadhar;
        this.slot = null;
        //this.conn = new ArrayList<>();

    }

    public User() {
        this.name = "";
        this.mob = "";
        this.aadhar = "";
        this.age = 0;
        this.dosesTaken = 0;
        this.vaccine = Null;
        //this.conn = new ArrayList<>();

    }

    public int getDosesTaken() {
        return dosesTaken;
    }

    public void setDosesTaken(int dosesTaken) {
        this.dosesTaken = dosesTaken;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }
//private ArrayList<String> conn;

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    public void addConnections(User user) {
        user.setMob(this.getMob());
        this.connections.add(user);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String toString() {
        return this.name + " " + this.age + "years  " + this.mob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String status() {
        if (slot == null)
            return "Doses taken: " + this.dosesTaken + "/n Not Scheduled for Vaccination";
        else
            return "Doses taken: " + this.dosesTaken + "/nScheduled at " + this.slot;
    }

    //public ArrayList<String> getConn() {
    //return conn;
    //}

    // public void setConn(ArrayList<String> conn) {
    ///this.conn = conn;
    //}
    public void vaccinate() {

        this.dosesTaken++;

    }

}
