package org.example.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Location {
    private String state;
    private String dist;
    private String locality;
    private String pincode;



    public String getState() {
        return state;
    }
    public void setLocation(String state, String dist, String locality, String pincode){
        this.state = state;
        this.dist = dist;
        this.locality = locality;
        this.pincode = pincode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    public String toString(){
        return this.locality + "," + this.dist + "," + this.state + "," + this.pincode;
    }

    public List<Vaccentre> getNearestVaccentres(List<Vaccentre> vaccentreDB){
        ArrayList<Vaccentre> nearest = new ArrayList<>();
        for(Vaccentre vacc : vaccentreDB) {
            Location loc2 = vacc.getLocation();
            if (this.locality.equals(loc2.locality)) {
                if (!nearest.contains(vacc))
                    nearest.add(vacc);
            }
        }
        for(Vaccentre vacc : vaccentreDB) {
            Location loc2 = vacc.getLocation();
            if (this.pincode.equals(loc2.pincode)) {
                if (!nearest.contains(vacc))
                    nearest.add(vacc);
            }
        }
        for(Vaccentre vacc : vaccentreDB) {
            Location loc2 = vacc.getLocation();
            if (this.dist.equals(loc2.dist)) {
                if (!nearest.contains(vacc))
                    nearest.add(vacc);
            }
        }
        for(Vaccentre vacc : vaccentreDB) {
            Location loc2 = vacc.getLocation();
            if (this.state.equals(loc2.state)) {
                if (!nearest.contains(vacc))
                    nearest.add(vacc);
            }
        }
        return nearest;

    }

    public Location getLocation(Scanner scanner){
        Location loc = new Location();
        System.out.print("Enter State:");
        loc.setState(scanner.nextLine());
        System.out.print("Enter District: ");
        loc.setDist(scanner.nextLine());
        System.out.print("Enter locality: ");
        loc.setLocality(scanner.nextLine());
        System.out.print("Enter Pincode: ");
        loc.setPincode(scanner.nextLine());
        return loc;
    }
}
