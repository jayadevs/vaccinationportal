package org.example.model;

import java.util.List;

public class Admin {
    private List<User> userDB;
    private List<Vaccentre> vaccentreDB;
    public String addVaccentre(Vaccentre vaccentre){
        if(!this.vaccentreDB.contains(vaccentre)) {
            this.vaccentreDB.add(vaccentre);
            return vaccentre.toString() + "added";
        }
        return "Failed to add vaccentre";
    }
    public void addStock(int stock, Vaccentre vaccentre){
        vaccentre.addStock(stock);
    }
    public void removeStock(int stock, Vaccentre vaccentre){
        vaccentre.removeStock(stock);
    }
    public void vaccinate(User user){
        Vaccentre vaccentre = user.getSlot().getVaccentre();
        vaccentre.vaccinate(user);
    }

}
