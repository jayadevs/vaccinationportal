package org.example.model;

import java.util.*;

public class Slot {
    private Date date;
    private String time;
    private Vaccentre vaccentre;

    public Vaccentre getVaccentre() {
        return vaccentre;
    }

    public void setVaccentre(Vaccentre vaccentre) {
        this.vaccentre = vaccentre;
    }



    public Slot(Date date, String time, Vaccentre vaccentre){

        this.date = date;
        this.time = time;
        this.vaccentre = vaccentre;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    public String toString(){
        return this.vaccentre.toString() + " " + this.time + " " + this.date;
    }


}
