package org.example;

import org.example.model.Location;
import org.example.model.Vaccentre;
import org.example.model.Vaccine;

import java.util.List;
import java.util.Scanner;

public class AdminDash {



    public void start(List<Vaccentre> vaccentreDB, Scanner scanner) {
        while (true){
            System.out.println("Manage Vaccentres.");
        System.out.println("Search a vaccination centre?: Press 1");
        System.out.println("Add new vaccination centre. Press 2. Press 3 to exit");
        int input = Integer.valueOf(scanner.nextLine());
        if (input == 3)
            break;

        if (input == 1) {
            System.out.print("Enter name of vaccentre: ");
            String name = scanner.nextLine();
            Vaccentre vaccentre = findVaccenter(name, vaccentreDB);


            while (true) {
                System.out.println("Add Stock?: Press 1");
                System.out.println("Remove stock?: Press 2");
                System.out.println("Remove vaccentre?: Press 3. Press 4 to exit");
                int ans = Integer.valueOf(scanner.nextLine());
                if (ans == 4)
                    break;
                if (ans == 1) {
                    System.out.print("How much?: ");
                    int newstock = Integer.valueOf(scanner.nextLine());
                    vaccentre.setStock(vaccentre.getStock() + newstock);

                }
                if (ans == 2) {
                    System.out.println("How much?");
                    int newstock = Integer.valueOf(scanner.nextLine());
                    newstock = newstock > vaccentre.getStock() ? (vaccentre.getStock()) : newstock;
                    vaccentre.setStock(vaccentre.getStock() - newstock);
                }
                if (ans == 3) {
                    vaccentreDB.remove(vaccentre);
                }

            }
        } else if (input == 2) {
            Location location = getLocation();
            System.out.print("Enter name of vaccination centre: ");
            String name = scanner.nextLine();
            System.out.print("Enter Vaccine name: ");
            Vaccine vaccine = Vaccine.valueOf(scanner.nextLine());
            System.out.print("Enter Stock: ");
            int stock = Integer.valueOf(scanner.nextLine());
            Vaccentre vacc = new Vaccentre(name, vaccine, location);
            vaccentreDB.add(vacc);


        }

    }
    }
    public Location getLocation(){
        Scanner reader = new Scanner(System.in);
        Location loc = new Location();
        System.out.print("Enter State: ");
        loc.setState(reader.nextLine());
        System.out.print("Enter District: ");
        loc.setDist(reader.nextLine());
        System.out.print("Enter locality: ");
        loc.setLocality(reader.nextLine());
        System.out.print("Enter Pincode: ");
        loc.setPincode(reader.nextLine());
        return loc;
    }

    public Vaccentre findVaccenter(String name, List<Vaccentre> vaccentres){
        for(Vaccentre vacc : vaccentres){
            if(vacc.getName().equals(name))
                return vacc;
        }
        return null;
    }



    //public Vaccentre isInDatabase(String name){

    //}//
}
