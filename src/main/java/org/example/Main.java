package org.example;

import org.example.model.Location;
import org.example.model.User;
import org.example.model.Vaccentre;
import org.example.model.Vaccine;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        ArrayList<User> userDB = new ArrayList<>();
        ArrayList<Vaccentre> vaccentreDB = new ArrayList<>();
        readFromUserFile(userDB);
        readFromVacFile(vaccentreDB);
        HomeScreen home = new HomeScreen();
        Scanner scanner = new Scanner((System.in));
        home.start(userDB,vaccentreDB, scanner );
        System.out.println("Exited Home");
        writeToUserFile(userDB);
        writeToVacFile(vaccentreDB);


    }

    public static void readFromUserFile(ArrayList<User> userDB){
        try(Scanner reader = new Scanner(Paths.get("Users.txt"))){
            while(reader.hasNextLine()){
                String[] parts = reader.nextLine().split(",");
                User user = new User();
                user.setMob(parts[0]);
                user.setName(parts[1]);
                user.setAadhar(parts[2]);
                user.setAge(Integer.parseInt(parts[3]));
                user.setDosesTaken(Integer.parseInt(parts[4]));
                user.setVaccine(Vaccine.valueOf(parts[5]));
                userDB.add(user);

            }

        }

        catch (IOException e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
    }

    public static void readFromVacFile(ArrayList<Vaccentre> vaccentres){
        try(Scanner reader = new Scanner(Paths.get("Vaccentres.txt"))){
            while(reader.hasNextLine()){
                String[] parts = reader.nextLine().split(",");
                Vaccentre vaccentre = new Vaccentre();
                vaccentre.setName(parts[0]);
                vaccentre.setVaccine(Vaccine.valueOf(parts[1]));
                Location loc  = new Location();
                loc.setLocality(parts[2]);
                loc.setDist(parts[3]);
                loc.setState(parts[4]);
                loc.setPincode(parts[5]);
                vaccentre.setLocation(loc);
                vaccentre.setStock(Integer.parseInt(parts[6]));
                vaccentres.add(vaccentre);

            }

        }

        catch (IOException e) {
            System.out.println("Error reading from file: " + e.getMessage());
        }
    }

    public static void writeToUserFile(ArrayList<User> users){
        try(FileWriter myWriter = new FileWriter("Users.txt")){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(User user : users) {
                bw.write(user.getMob() + "," + user.getName() + "," + user.getAadhar() + "," + user.getAge()  + "," + user.getDosesTaken() + "," + user.getVaccine());
                bw.newLine();

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }

    public static void writeToVacFile(ArrayList<Vaccentre> vaccentres){
        try(FileWriter myWriter = new FileWriter("Vaccentres.txt")){
            BufferedWriter bw = new BufferedWriter(myWriter);
            for(Vaccentre vaccentre : vaccentres) {
                bw.write(vaccentre.getName() + "," + vaccentre.getVaccine() + "," + vaccentre.getLocation().toString() + "," + vaccentre.getStock());
                bw.newLine();

            }
            bw.close();
            System.out.println("Successfully wrote to the file.");

        }
        catch(Exception e){
            System.out.println("Error" + e.getMessage());

        }
    }



}
